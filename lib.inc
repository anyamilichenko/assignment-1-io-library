section .text 
%define EXIT_CALL_CONSTANT 60
%define WRITE_SYSCALL_NUMBER 1
%define STD_OUT 1


; Принимает код возврата и завершает текущий процесс
exit:
  mov rax, EXIT_CALL_CONSTANT
  syscall 


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
  xor rax, rax
  .counter:
    inc rax
    cmp byte[rdi+rax-1], 0
    jne .counter
  dec rax
  ret



; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
  push rdi
  call string_length
  pop rsi ; pointer
  mov  rdx, rax ; length
  mov  rax, WRITE_SYSCALL_NUMBER
  mov  rdi, STD_OUT
  syscall
  ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
  mov rdi, '\n'


; Принимает код символа и выводит его в stdout
print_char:
  push rdi
  mov  rsi, rsp 
  mov  rdx, 1
  mov  rax, WRITE_SYSCALL_NUMBER
  mov  rdi, STD_OUT
  syscall
  pop rdi
  ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
   	test rdi, rdi
	jns print_uint
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.

print_uint:
    xor rdx, rdx
    push rdx    
    xor r10, r10
    mov r8, 10    
    mov rax, rdi
    .division:
        dec rsp
        inc r10
        div r8
        add rdx, "0"
        mov [rsp], dl
        xor rdx, rdx
		test rax, rax
        jnz .division
    mov rsi, rsp    
    mov rdi, STD_OUT
    mov rdx, r10     
    mov rax, WRITE_SYSCALL_NUMBER   
    syscall
    add rsp, r10    
    add rsp, 8
    ret






; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax ; по умолч вернет 0
    xor rcx, rcx
    .loop:
        inc rcx
        mov r8b, byte[rdi+rcx-1] 
        cmp r8b, byte[rsi+rcx-1]
        je .check_null
        ret 
    .check_null:
        test r8b, byte[rsi+rcx] 
        jnz .loop
        inc rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
        push    r13
		mov r13, rdi
        push    r14
        push    r15
        xor    r14, r14
        mov    r15, rsi
       
        dec    r15

        cmp    rsi, 1
        jle    .exception

        .white:
            call    read_char    
            cmp    al, ' '
            je    .white
            cmp    al, `\n`
            je    .white
            cmp    al, `\t`
            je    .white
            test    al, al
            jz    .success

        .chars:
            mov    byte[ r13 + r14 ], al
            inc     r14
            call    read_char
            cmp    al, ' '
            je    .success
            cmp    al, `\n`
            je    .success
            cmp    al, `\t`
            je    .success
            test    al, al
            jz    .success
            cmp    r14, r15
            je    .exception

            jmp    .chars

        .exception:
            xor    rax, rax
            jmp    .end
           
        .success:
            mov    byte[ r13 + r14 ], 0
            mov    rax, r13
            mov    rdx, r14
           
        .end:
			pop r13
            pop    r15
            pop     r14
            ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:
    push rcx
    push r10

    xor rcx, rcx ; 0 -> rcx (length)
    xor rax, rax ; 0 -> rax (result)
    xor rdx, rdx
    mov r10, 10

parse_uint_next:
    cmp byte[rdi+rcx], '0'
    jl parse_uint_ret
    cmp byte[rdi+rcx], '9'
    jg parse_uint_ret
    mul r10
    mov dl,  byte[rdi+rcx]
    sub dl, '0'
    add rax, rdx
    inc rcx
    jmp parse_uint_next

parse_uint_ret:
    mov rdx, rcx
    pop rcx
    pop r10
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:    
	cmp byte[rdi], '-'    
	jne parse_uint    
parse_int_neg:
	inc rdi    
	call parse_uint    
	neg rax    
	inc rdx
	ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
    push rbx
    xor rax, rax
    .loop:
    	cmp rax, rdx 
    	je .notfit
    	mov bl, [rdi + rax]
    	mov byte[rsi + rax], bl
    	test bl, bl
    	je .fit
    	inc rax
    	jmp .loop
    .notfit:
    	xor rax, rax
    .fit:
    	pop rbx
    	ret
